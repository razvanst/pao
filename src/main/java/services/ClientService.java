package services;

import models.Client;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClientService extends DatabaseService {
    public List<Client> getAllClients() {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Client> clients = new ArrayList<>();
        String sql = "SELECT * FROM clients";

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {

                clients.add( new Client(
                        rs.getInt("client_id"),
                        rs.getString("client_name"),
                        rs.getInt("cnp")
                ) );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return clients;
    }

    public Client getClient(int clientId) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sql = "SELECT * FROM clients WHERE client_id="+clientId;
        Client client = new Client();

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                client = new Client(
                    rs.getInt("client_id"),
                    rs.getString("client_name"),
                    rs.getInt("cnp")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return client;
    }
}
