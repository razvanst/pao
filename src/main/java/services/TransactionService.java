package services;

import com.sun.istack.internal.Nullable;
import models.ApiTransaction;
import models.Transaction;

import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TransactionService extends DatabaseService {

    public List<ApiTransaction> getAllTransactions(String order) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<ApiTransaction> transactions = new ArrayList<>();
        ClientService clientService = new ClientService();
        CurencyService curencyService = new CurencyService();
        String sql = "SELECT transactions.*,client_name,c1.curency_label as sell_curency_label,c2.curency_label as buy_curency_label " +
                "FROM transactions " +
                "INNER JOIN clients USING(client_id) " +
                "INNER JOIN curencies as c1 ON transactions.sell_curency=c1.curency_id " +
                "INNER JOIN curencies as c2 ON transactions.buy_curency=c2.curency_id " +
                "ORDER BY transaction_id " + order;

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                Date date=new SimpleDateFormat("yyyy-MM-dd").parse( rs.getString("transaction_date") );
                transactions.add( new ApiTransaction(
                        rs.getInt("transaction_id"),
                        rs.getInt("client_id"),
                        rs.getString("client_name"),
                        rs.getString("sell_curency_label"),
                        rs.getString("buy_curency_label"),
                        rs.getFloat("sell_amount"),
                        rs.getFloat("buy_amount"),
                        date
                ) );
            }
        } catch (SQLException | ParseException e) {
            System.out.println(e.getMessage());
        }

        return transactions;
    }

    public List<Transaction> getTransactions(int clientId, String order) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Transaction> transactions = new ArrayList<>();
        String sql = "SELECT * FROM transactions WHERE client_id="+clientId+" ORDER BY transaction_id " + order;

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                Date date=new SimpleDateFormat("yyyy-MM-dd").parse( rs.getString("transaction_date") );
                transactions.add( new Transaction(
                        rs.getInt("transaction_id"),
                        rs.getInt("client_id"),
                        rs.getInt("sell_curency"),
                        rs.getInt("buy_curency"),
                        rs.getFloat("sell_amount"),
                        rs.getFloat("buy_amount"),
                        date
                ) );
            }
        } catch (SQLException | ParseException e) {
            System.out.println(e.getMessage());
        }

        return transactions;
    }

    public int addTransaction( Transaction transaction ) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sql = "INSERT INTO transactions VALUES(null, ?, ?, ?, ?, ?, ?)";
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, transaction.getClient_id());
            pstmt.setInt(2, transaction.getSell_curency());
            pstmt.setInt(3, transaction.getBuy_curency());
            pstmt.setFloat(4, transaction.getSell_amount());
            pstmt.setFloat(5, transaction.getBuy_amount());
            pstmt.setString(6, dateFormat.format(date));

            if( pstmt.executeUpdate() > 0 ) {
                ResultSet rs = pstmt.getGeneratedKeys();
                rs.next();
                int pk = rs.getInt(1);
                return pk;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return -1;
    }
}
