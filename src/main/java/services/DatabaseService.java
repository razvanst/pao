package services;

import java.io.IOException;
import java.sql.*;

public class DatabaseService extends Service {


    protected Connection connect() {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // SQLite connection string
        String url = "jdbc:sqlite:database.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    protected void printResultSet(ResultSet resultSet) throws SQLException {
        ResultSetMetaData rsmd = resultSet.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        while (resultSet.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = resultSet.getString(i);
                System.out.print(columnValue + " " + rsmd.getColumnName(i));
            }
            System.out.println("");
        }
    }

}
