package services;

import models.Curency;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CurencyService extends DatabaseService {

    public List<Curency> getAllCurencies() {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Curency> curencies = new ArrayList<>();
        String sql = "SELECT * FROM curencies";

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {

                curencies.add( new Curency(
                        rs.getInt("curency_id"),
                        rs.getString("curency_name"),
                        rs.getString("curency_label")
                ) );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return curencies;
    }

    public Curency getCurency(String curencyLabel ) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Curency curency = new Curency();
        String sql = "SELECT * FROM curencies WHERE curency_label='"+curencyLabel+"'";

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                curency = new Curency(
                        rs.getInt("curency_id"),
                        rs.getString("curency_name"),
                        rs.getString("curency_label")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return curency;
    }

    public Curency getCurency(int curencyId ) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Curency curency = new Curency();
        String sql = "SELECT * FROM curencies WHERE curency_id='"+curencyId+"'";

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){

            // loop through the result set
            while (rs.next()) {
                curency = new Curency(
                        rs.getInt("curency_id"),
                        rs.getString("curency_name"),
                        rs.getString("curency_label")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return curency;
    }

}
