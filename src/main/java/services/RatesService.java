package services;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class RatesService extends Service{

    public Float getRateFor( String sell, String buy ) {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sURL = "https://api.exchangeratesapi.io/latest?base="+sell+"&symbols="+buy; //just a string

        // Connect to the URL using java's native library
        URL url = null;
        try {
            url = new URL(sURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLConnection request = null;
        try {
            request = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            request.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Convert to a JSON object to print data
        JsonParser jp = new JsonParser(); //from gson
        JsonElement root = null; //Convert the input stream to a json element
        try {
            root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.
        Float rate = rootobj.get("rates").getAsJsonObject().get(buy).getAsFloat();
        return rate;
    }

    public String getRONrates() {
        //Log action to CSV
        try {
            String className = new Object(){}.getClass().getName();
            String methodName = new Object(){}.getClass().getEnclosingMethod().getName();
            csvService.writeToCsv(className + "." + methodName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String sURL = "https://api.exchangeratesapi.io/latest?base=RON"; //just a string

        // Connect to the URL using java's native library
        URL url = null;
        try {
            url = new URL(sURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLConnection request = null;
        try {
            request = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            request.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Convert to a JSON object to print data
        JsonParser jp = new JsonParser(); //from gson
        JsonElement root = null; //Convert the input stream to a json element
        try {
            root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.
        String rate = rootobj.get("rates").toString();
        return rate;
    }

}
