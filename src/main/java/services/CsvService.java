package services;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

public class CsvService {

    private String logPath;

    public CsvService() {
        this.logPath = "log.csv";
    }

    private boolean isLog() {
        File csvFile = new File(this.logPath);
        if (csvFile.isFile()) {
            return true;
        }
        return false;
    }

    private void createCsv() throws IOException {
        if( this.isLog() ) return;
        FileWriter csvWriter = new FileWriter(this.logPath);
        csvWriter.append("action");
        csvWriter.append(",");
        csvWriter.append("timestamp");
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
    }

    public void writeToCsv(String action) throws IOException {
        this.createCsv();
        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);
        String timeStr = ts.toString();

        FileWriter csvWriter = new FileWriter(this.logPath, true);
        csvWriter.append(action);
        csvWriter.append(",");
        csvWriter.append(timeStr);
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
    }

}
