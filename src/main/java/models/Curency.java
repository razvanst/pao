package models;

public class Curency {
    private int curency_id;
    private String curency_name;
    private String curency_label;

    public Curency() {
    }

    public Curency(int curency_id, String curency_name, String curency_label) {
        this.curency_id = curency_id;
        this.curency_name = curency_name;
        this.curency_label = curency_label;
    }

    public int getCurency_id() {
        return curency_id;
    }

    public void setCurency_id(int curency_id) {
        this.curency_id = curency_id;
    }

    public String getCurency_name() {
        return curency_name;
    }

    public void setCurency_name(String curency_name) {
        this.curency_name = curency_name;
    }

    public String getCurency_label() {
        return curency_label;
    }

    public void setCurency_label(String curency_label) {
        this.curency_label = curency_label;
    }

    @Override
    public String toString() {
        return "Curency{" +
                "curency_id=" + curency_id +
                ", curency_name='" + curency_name + '\'' +
                ", curency_label='" + curency_label + '\'' +
                '}';
    }
}
