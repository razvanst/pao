package models;

import java.util.Date;

public class Transaction {
    private int transaction_id;
    private int client_id;
    private int sell_curency, buy_curency;
    private float sell_amount, buy_amount;
    private Date transaction_date;

    public Transaction() {
    }

    public Transaction(int transaction_id, int client_id, int sell_curency, int buy_curency, float sell_amount, float buy_amount, Date transaction_date) {
        this.transaction_id = transaction_id;
        this.client_id = client_id;
        this.sell_curency = sell_curency;
        this.buy_curency = buy_curency;
        this.sell_amount = sell_amount;
        this.buy_amount = buy_amount;
        this.transaction_date = transaction_date;
    }

    public Transaction(int client_id, int sell_curency, int buy_curency, float sell_amount, float buy_amount) {
        this.client_id = client_id;
        this.sell_curency = sell_curency;
        this.buy_curency = buy_curency;
        this.sell_amount = sell_amount;
        this.buy_amount = buy_amount;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public int getSell_curency() {
        return sell_curency;
    }

    public void setSell_curency(int sell_curency) {
        this.sell_curency = sell_curency;
    }

    public int getBuy_curency() {
        return buy_curency;
    }

    public void setBuy_curency(int buy_curency) {
        this.buy_curency = buy_curency;
    }

    public float getSell_amount() {
        return sell_amount;
    }

    public void setSell_amount(float sell_amount) {
        this.sell_amount = sell_amount;
    }

    public float getBuy_amount() {
        return buy_amount;
    }

    public void setBuy_amount(float buy_amount) {
        this.buy_amount = buy_amount;
    }

    public Date getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Date transaction_date) {
        this.transaction_date = transaction_date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "transaction_id=" + transaction_id +
                ", client_id=" + client_id +
                ", sell_curency=" + sell_curency +
                ", buy_curency=" + buy_curency +
                ", sell_amount=" + sell_amount +
                ", buy_amount=" + buy_amount +
                ", transaction_type='" + transaction_date + '\'' +
                '}';
    }
}
