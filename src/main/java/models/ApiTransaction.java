package models;

import java.util.Date;

public class ApiTransaction {
    private int transaction_id;
    private int client_id;
    private String client_name;
    private String sell_curency, buy_curency;
    private float sell_amount, buy_amount;
    private Date transaction_date;

    public ApiTransaction(int transaction_id, int client_id, String client_name, String sell_curency, String buy_curency, float sell_amount, float buy_amount, Date transaction_date) {
        this.transaction_id = transaction_id;
        this.client_id = client_id;
        this.client_name = client_name;
        this.sell_curency = sell_curency;
        this.buy_curency = buy_curency;
        this.sell_amount = sell_amount;
        this.buy_amount = buy_amount;
        this.transaction_date = transaction_date;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getSell_curency() {
        return sell_curency;
    }

    public void setSell_curency(String sell_curency) {
        this.sell_curency = sell_curency;
    }

    public String getBuy_curency() {
        return buy_curency;
    }

    public void setBuy_curency(String buy_curency) {
        this.buy_curency = buy_curency;
    }

    public float getSell_amount() {
        return sell_amount;
    }

    public void setSell_amount(float sell_amount) {
        this.sell_amount = sell_amount;
    }

    public float getBuy_amount() {
        return buy_amount;
    }

    public void setBuy_amount(float buy_amount) {
        this.buy_amount = buy_amount;
    }

    public Date getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Date transaction_date) {
        this.transaction_date = transaction_date;
    }

    @Override
    public String toString() {
        return "ApiTransaction{" +
                "transaction_id=" + transaction_id +
                ", client_id=" + client_id +
                ", client_name='" + client_name + '\'' +
                ", sell_curency='" + sell_curency + '\'' +
                ", buy_curency='" + buy_curency + '\'' +
                ", sell_amount=" + sell_amount +
                ", buy_amount=" + buy_amount +
                ", transaction_date=" + transaction_date +
                '}';
    }
}
