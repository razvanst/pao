package api;

import com.google.gson.JsonObject;
import models.ApiTransaction;
import models.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import services.TransactionService;

import java.util.List;

@RestController
public class TransactionController {

    @GetMapping(path = {"/getAllTransactions", "/getAllTransactions/{order}"})
    public List<ApiTransaction> getAllTransactions(@PathVariable(name = "order", required = false) String order) {
        order = (order == null ? "ASC" : order);
        TransactionService app = new TransactionService();
        return app.getAllTransactions(order);
    }

    @GetMapping(path = {"/getTransactions/{clientId}", "/getTransactions/{clientId}/{order}"})
    public List<Transaction> getTransactions(@PathVariable int clientId, @PathVariable(name = "order", required = false) String order) {
        order = (order == null ? "ASC" : order);
        TransactionService app = new TransactionService();
        return app.getTransactions(clientId, order);
    }

    @PostMapping("/addTransaction")
    public ResponseEntity<String> addTransaction(@RequestBody Transaction newTransaction) {
        TransactionService app = new TransactionService();
        int insertStatus = app.addTransaction(newTransaction);

        JsonObject returnObj = new JsonObject();
        returnObj.addProperty("transaction_id", insertStatus);
        String returnStr = returnObj.toString();

        if( insertStatus >= 0 )
            return new ResponseEntity<String>(returnStr, HttpStatus.OK);
        return new ResponseEntity<String>(returnStr, HttpStatus.BAD_REQUEST);
    }
}
