package api;

import models.Client;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import services.ClientService;

import java.util.List;

@RestController
public class ClientController {

    @GetMapping("/getClients")
    public List<Client> getClients() {
        ClientService app = new ClientService();
        return app.getAllClients();
    }

    @GetMapping("/getClient/{clientId}")
    public Client getClient(@PathVariable int clientId) {
        ClientService app = new ClientService();
        return app.getClient(clientId);
    }
}
