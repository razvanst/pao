package api;

import com.google.gson.JsonObject;
import models.Curency;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import services.CurencyService;
import services.RatesService;

import java.util.List;

@RestController
public class CurencyController {

    @GetMapping("/getCurencies")
    public List<Curency> getCurencies() {
        CurencyService curencyService = new CurencyService();
        return curencyService.getAllCurencies();
    }

    @GetMapping("/getCurency/{curencyLabel}")
    public Curency getCurency(@PathVariable String curencyLabel) {
        CurencyService curencyService = new CurencyService();
        try
        {
            Integer.parseInt(curencyLabel);
            return curencyService.getCurency( Integer.parseInt(curencyLabel) );

        } catch (NumberFormatException ex)
        {
            return curencyService.getCurency(curencyLabel);
        }
    }

    @GetMapping("/getRatesFor/{c1}/{c2}")
    public String getRatesFor(@PathVariable String c1, @PathVariable String c2) {
        RatesService ratesService = new RatesService();

        JsonObject returnObj = new JsonObject();
        returnObj.addProperty("rate", ratesService.getRateFor(c1, c2));
        String returnStr = returnObj.toString();

        return returnStr;
    }

    @GetMapping("/getRONrates")
    public String getRONrates() {
        RatesService ratesService = new RatesService();
        return ratesService.getRONrates();
    }
}
