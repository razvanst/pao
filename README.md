#API Routes#

##Client
**GET** `/getClients` - get all clients

**GET** `/getClient/{client ID}` - get a specific client

##Curency
**GET** `/getCurencies` - get all supported curencies

**GET** `/getCurency/{curency label}` - get a curency with a specific label

**GET** `/getRatesFor/{c1}/{c2}` - Get the rate for c1 and c2 curencies

**GET** `/getRONrates` - Get all rates for RON

##Transactions
**GET** `/getAllTransactions` or `/getAllTransactions/{order}` - get all transactions

**GET** `/getTransactions/{client ID}` or `/getTransactions/{client ID}/{order}` - get transactions for a specific client

**POST** `/addTransaction` - add a transaction
 